from setuptools import setup, find_packages

setup(
    name='model-facebook',
    version='0.20',
    description='Meltano .m5o models for Ads data fetched using the Facebook Marketing API',
    packages=find_packages(),
    package_data={'models': ['**/*.m5o', '*.m5o']},
    install_requires=[],
)
