{
  include "mixins/insights_date.m5o"

  version = 1
  sql_table_name = fb_ads_insights_age_and_gender
  name = fb_ads_insights_age_and_gender
  columns {
    account_id {
      label = Account ID
      hidden = yes
      type = string
      sql = "{{TABLE}}.account_id"
    }
    campaign_id {
      label = Campaign ID
      hidden = yes
      type = string
      sql = "{{TABLE}}.campaign_id"
    }
    adset_id {
      label = Adset ID
      hidden = yes
      type = string
      sql = "{{TABLE}}.adset_id"
    }
    ad_id {
      label = Ad ID
      hidden = yes
      type = string
      sql = "{{TABLE}}.ad_id"
    }
    age {
      label = Age (User Demographics)
      description = Age (User Demographics)
      type = string
      sql = "{{table}}.age"
    }
    gender {
      label = Gender (User Demographics)
      description = Gender (User Demographics)
      type = string
      sql = "{{table}}.gender"
    }
    account_name {
      label = Account Name
      description = Account Name
      type = string
      sql = "{{table}}.account_name"
    }
    campaign_name {
      label = Campaign Name
      description = Campaign Name
      type = string
      sql = "{{table}}.campaign_name"
    }
    adset_name {
      label = Adset Name
      description = Adset Name
      type = string
      sql = "{{table}}.adset_name"
    }
    ad_name {
      label = Ad Name
      description = Ad Name
      type = string
      sql = "{{table}}.ad_name"
    }
    insights_year {
      label = Report Year
      description = Report Year
      type = string
      sql = "{{table}}.insights_year"
    }
    insights_quarter {
      label = Report Quarter
      description = Report Quarter
      type = string
      sql = "{{table}}.insights_quarter"
    }
    insights_month {
      label = Report Month
      description = Report Month
      type = string
      sql = "{{table}}.insights_month"
    }
    insights_week {
      label = Report Week
      description = Report Week
      type = string
      sql = "{{table}}.insights_week"
    }
    insights_day {
      label = Report Day
      description = Report Day
      type = string
      sql = "{{table}}.insights_day"
    }
    objective {
      label = Campaign Objective
      description = Campaign Objective
      type = string
      sql = "{{table}}.objective"
    }
  }
  aggregates {
    spend {
      label = Spend
      description = "The estimated total amount of money you've spent on your campaign, ad set or ad during its schedule"
      type = sum
      sql = "{{table}}.spend"
    }
    social_spend {
      label = Social Spend
      description = "The total amount you've spent so far for your ads showed with social information"
      type = sum
      sql = "{{table}}.social_spend"
    }
    inline_link_clicks {
      label = Inline Link Clicks
      description = "The number of clicks on links to select destinations or experiences"
      type = sum
      sql = "{{table}}.inline_link_clicks"
    }
    unique_inline_link_clicks {
      label = Unique Inline Link Clicks
      description = Unique Inline Link Clicks
      type = sum
      sql = "{{table}}.unique_inline_link_clicks"
    }
    clicks {
      label = Clicks (All)
      description = "The number of clicks on your ads"
      type = sum
      sql = "{{table}}.clicks"
    }
    unique_clicks {
      label = Unique Clicks (All)
      description = "The number of people who performed a click (all)"
      type = sum
      sql = "{{table}}.unique_clicks"
    }
    impressions {
      label = Impressions
      description = "The number of times your ads were on screen"
      type = sum
      sql = "{{table}}.impressions"
    }
    results {
      label = Results
      description = "The number of actions that are in accordance to the Objective of an Ad"
      type = sum
      sql = "{{table}}.results"
    }
    mobile_app_installs {
      label = Mobile App Installs
      description = "Mobile App Installs"
      type = sum
      sql = "{{table}}.actions_mobile_app_installs"
    }
    app_installs {
      label = Desktop App Installs
      description = "Desktop App Installs"
      type = sum
      sql = "{{table}}.actions_app_installs"
    }
    inline_post_engagement {
      label = Total Post Engagement
      description = "The total number of actions that people take involving your ads"
      type = sum
      sql = "{{table}}.inline_post_engagement"
    }
    post_reactions {
      label = Post Reactions
      description = "Post Reactions"
      type = sum
      sql = "{{table}}.actions_post_reactions"
    }
    post_comments {
      label = Post Comments
      description = "Post Comments"
      type = sum
      sql = "{{table}}.actions_post_comments"
    }
    post_saves {
      label = Post Saves
      description = "Post Saves"
      type = sum
      sql = "{{table}}.actions_post_saves"
    }
    post_shares {
      label = Post Shares
      description = "Post Shares"
      type = sum
      sql = "{{table}}.actions_post_shares"
    }
    page_likes {
      label = Page Likes
      description = "Page Likes"
      type = sum
      sql = "{{table}}.actions_page_likes"
    }
    messages {
      label = Messages
      description = "Messaging Conversations Started"
      type = sum
      sql = "{{table}}.actions_messages"
    }
    event_responses {
      label = Event Responses (RSVP)
      description = "Event Responses (RSVP)"
      type = sum
      sql = "{{table}}.actions_event_responses"
    }
    video_views {
      label = Video Views
      description = "Video Views"
      type = sum
      sql = "{{table}}.actions_video_views"
    }
  }
}
